<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>


<?php do_action( 'woocommerce_product_meta_start' ); ?>

    <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
        <div class="span span1">
            <p class="left">
                <?php
                // Вывод строки SKU с переводом по словарю локализации
                esc_html_e( 'SKU:', 'woocommerce' );
                ?></p>
            <p class="right"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></p>
            <div class="clearfix"></div>
        </div>
    <?php endif; ?>

    <div class="span span2">
        <?php
        // Вывод строки категории товара с переводом по словарю локализации
        echo wc_get_product_category_list( $product->get_id(), ', ', '<p class="left">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . '</p><p class="right">', '</p>' );
        ?>
        <div class="clearfix"></div>
    </div>
    <div class="span span3">
        <?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<p class="left">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . '</p><p class="right">', '</p>' ); ?>
        <div class="clearfix"></div>
    </div>

<?php do_action( 'woocommerce_product_meta_end' ); ?>
