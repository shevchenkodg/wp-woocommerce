<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Информация по текущему продукту, информацию которого мы получаем от текущей
// итерации цикла
global $product;
?>
<!--
<div class="col-md-4 product simpleCart_shelfItem text-center">
    <a href="single.html"><img src="<?php bloginfo('template_directory'); ?>/images/p1.jpg" alt="" /></a>
    <div class="mask">
        <a href="single.html">Quick View</a>
    </div>
    <a class="product_name" href="single.html">Sed ut perspiciatis</a>
    <p><a class="item_add" href="#"><i></i> <span class="item_price">$329</span></a></p>
</div>
-->

<div class="col-md-4 product simpleCart_shelfItem text-center">
    <?php do_action('woocommerce_before_shop_loop_item'); ?>
    <?php do_action('woocommerce_before_shop_loop_item_title'); ?>
    <?php do_action('woocommerce_shop_loop_item_title'); ?>
    <?php
    // цена
    // do_action('woocommerce_after_shop_loop_item_title'); ?>
    <?php do_action('woocommerce_after_shop_loop_item'); ?>
</div>
