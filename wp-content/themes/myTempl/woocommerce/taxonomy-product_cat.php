<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Шапка
get_header();
// Навигационная панель
get_navigation();
?>

<div class="container">
	<div class="products-page">
        <?php get_sidebar(); ?>
		<div class="new-product">

            <?php remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10); ?>
            <?php do_action('woocommerce_before_main_content'); ?>

 			<div class="mens-toolbar">

                <?php do_action('woocommerce_before_shop_loop'); ?>

				<div class="clearfix"></div>
			</div>

			<div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
				<div class="cbp-vm-options">
					<a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid" title="grid">Grid View</a>
					<a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list" title="list">List View</a>
				</div>
				<div class="pages">
					<div class="limiter visible-desktop">
						<label>Show</label>
						<select id="pagination-posts">
							<option <?php if(isset($_GET['ppp']) && $_GET['ppp'] == 3):?>selected="selected"<?php endif;?> value="3">
								3                </option>
							<option <?php if(isset($_GET['ppp']) && $_GET['ppp'] == 6):?>selected="selected"<?php endif;?> value="6">
								6                </option>
							<option <?php if($_GET['ppp'] == 9 || !isset($_GET['ppp'])):?>selected="selected"<?php endif;?> value="9">
								9                </option>
						</select> per page
					</div>
				</div>
				<div class="clearfix"></div>
                <!--content-->
                <?php if(have_posts()): ?>
				<ul class="cat-products">
                    <?php while(have_posts()): the_post(); ?>
                        <?php
                        /**
                         * woocommerce_shop_loop hook.
                         *
                         * @hooked WC_Structured_Data::generate_product_data() - 10
                         */
                        do_action( 'woocommerce_shop_loop' );
                        ?>
                        <?php wc_get_template_part('content', 'product-cat'); ?>
                    <?php endwhile; ?>
                </ul>
                <?php else: ?>
                    <?php wc_get_template('loop/no-products-found.php'); ?>
                <?php endif; ?>
            </div>
            <?php
            wp_enqueue_script('cbpViewModeSwitch', get_template_directory_uri().'/js/cbpViewModeSwitch.js');
            wp_enqueue_script('classie', get_template_directory_uri().'/js/classie.js');
            ?>

        </div>
        <?php do_action('woocommerce_after_shop_loop'); ?>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>

<?php
// Сайдбар 'Content Bottom'
get_sidebar('content-bottom');
// Футер
get_footer();
