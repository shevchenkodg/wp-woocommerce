<?php
// Защита от прямого доступа к текущему файлу
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;

// Если продукт не опубликован или пустой
if(empty($product) || !$product->is_visible()) {
	return;
}
?>

<li class="product">
	<?php
    // <a class="cbp-vm-image" href="single.html">
    do_action('woocommerce_before_shop_loop_item');
    do_action('woocommerce_before_shop_loop_item_title');
    ?>
        <?php
        do_action('woocommerce_shop_loop_item_title');
        do_action('woocommerce_after_shop_loop_item_title');
        ?>
    <?php
    // </a>
    // кнопка добавления в корзину
    do_action('woocommerce_after_shop_loop_item');
    ?>
</li>
