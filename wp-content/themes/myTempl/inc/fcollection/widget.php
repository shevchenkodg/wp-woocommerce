<?php
// Вешаем на хук widgets_init
add_action('widgets_init', 'f_collection');
// Регистрация виджета
function f_collection() {
    register_widget('F_Collection');
}

// Класс нового виджета
class F_Collection extends WP_Widget {
    // Метод-конструктор
    function F_Collection() {
        // Параметры виджета
        $widget_ops = array(
            'classname' => 'myTempl',
            'description' => 'Описание'
        );
        // Параметры панели виджета
        $control_ops = array(
            'width' => 300,
            'height' => 350
        );
        // Обращаемся к конструктору родительского класса
        $this->WP_Widget('fcollection', 'Featured Collection', $widget_ops, $control_ops);
    }

    // Механизм корректного сохранения значений виджета
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['pcount'] = strip_tags($new_instance['pcount']);

        return $instance;
    }

    // Метод отображения виджета в пользовательской части
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        $pcount = $instance['pcount'];

        include 'templ.php';
    }

    // Форма для настройки виджета
    function form($instance) {
        // Начальные параметры виджета (настройки по умолчанию)
        $dafaults = array(
            'title' => 'Featured Collection',
            // начальное количество товаров, отображаемых в виджете
            'pcount' => 9
        );
        // Обьединяем параметры массивов (значения первого массива заменяют значения второго),
        // $instance конвертируем в массив, так как он может быть обьектом
        $instance = wp_parse_args((array)$instance, $dafaults);
        ?>
        <p>
            <!-- Поле для определения заголовка виджета-->
            <label for="<?php echo $this->get_field_id('title') ?>">Title</label>
            <input id="<?php echo $this->get_field_id('title') ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" style="width: 100%;">
        </p>
        <p>
            <!-- Поле количества отображаемого товара-->
            <label for="<?php echo $this->get_field_id('pcount') ?>">Count Products</label>
            <input id="<?php echo $this->get_field_id('pcount') ?>" name="<?php echo $this->get_field_name('pcount'); ?>" value="<?php echo $instance['pcount']; ?>" style="width: 100%;">
        </p>
        <?php
    }
}