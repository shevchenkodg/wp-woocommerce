<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();

get_navigation();
?>
<h1>404</h1>
<?php
get_footer();